import React, { Component } from "react";

export default class ShowTodos extends Component {
  getStyle = (todo) => {
    const todoColor = todo.completed ? "#363" : "#633";
    return {
      color: "white",
      backgroundColor: todoColor,
      padding: "1rem 1rem",
      borderRadius: "0.5rem",
      textAlign: "left",
      display: "flex",
      justifyContent: "space-between",
    };
  };

  getIcon(completed) {
    return completed ? "far fa-times-circle" : "fas fa-check-circle";
  }

  handleClick = (id, completed) => {
    completed ? this.props.removeTodo(id) : this.props.completeTodo(id);
  };

  getUser(id) {}
  render() {
    return (
      <div>
        {this.props.todos.map((td, ind) => {
          return (
            <div key={td.id}>
              <p style={this.getStyle(td)}>
                <span style={{ flexGrow: "10" }}>
                  {ind + 1 + ". "} {td.title}
                </span>
                <span style={{ flexGrow: "0", padding: "0 1rem 0 0" }}>
                  {"  -" +
                    this.props.users.find((u) => u.id === td.userId).username}
                </span>
                <button
                  style={{
                    font: "inherit",
                    background: "none",
                    border: "none",
                    color: "inherit",
                  }}
                  onClick={() => this.handleClick(td.id, td.completed)}
                >
                  <i
                    stype={{ flexGrow: "0" }}
                    className={this.getIcon(td.completed)}
                  ></i>
                </button>
              </p>
            </div>
          );
        })}
      </div>
    );
  }
}
