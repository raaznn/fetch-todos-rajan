/* --------------------libraries----------------------*/
import React, { Component } from "react";

/* --------------------components --------------------*/
import ShowTodos from "./ShowTodos";

/*----------------------------------------------------*/

export default class FetchTodos extends Component {
  state = {
    loading: true,
    todos: [],
    users: [],
  };

  componentDidMount() {
    this.fetchData();
  }

  async fetchData() {
    this.setState({ loading: true });
    const [todos, todosError] = await this.fetchUrl(
      "https://jsonplaceholder.typicode.com/todos"
    );
    const [users, usersError] = await this.fetchUrl(
      "https://jsonplaceholder.typicode.com/users"
    );
    !todosError &&
      !usersError &&
      this.setState({
        todos: todos.sort(() => 0.5 - Math.random()).slice(0, 20),
        users: users,
        loading: false,
      });
  }

  async fetchUrl(url) {
    try {
      const response = await fetch(url);
      return [await response.json(), null];
    } catch (err) {
      console.log("Fetch Error ", err);
      return [null, err];
    }
  }

  removeTodo = (id) => {
    this.setState({
      todos: this.state.todos.filter((td) => td.id != id),
    });
  };

  completeTodo = (id) => {
    this.setState({
      todos: this.state.todos.map((td) => {
        return td.id == id ? { ...td, completed: true } : td;
      }),
    });
  };
  render() {
    return (
      <div className="App-header">
        <h1> Todos </h1>

        {this.state.loading ? (
          <h3> Loading todos... </h3>
        ) : (
          <ShowTodos
            todos={this.state.todos}
            users={this.state.users}
            removeTodo={this.removeTodo}
            completeTodo={this.completeTodo}
          />
        )}
      </div>
    );
  }
}
