/*----------------------CSS-------------------------*/
import "./App.css";

/*-------------------Components---------------------*/
import FetchTodos from "./components/FetchTodos";

/*--------------------------------------------------*/

function App() {
  return (
    <div className="App">
      <FetchTodos />
    </div>
  );
}

export default App;
